// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: article.proto

/*
Package article is a generated protocol buffer package.

It is generated from these files:
	article.proto

It has these top-level messages:
	Filter
	Article
	Articles
*/
package article

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	client "github.com/micro/go-micro/client"
	server "github.com/micro/go-micro/server"
	context "context"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ client.Option
var _ server.Option

// Client API for ArticleGetter service

type ArticleGetterService interface {
	GetArticles(ctx context.Context, in *Filter, opts ...client.CallOption) (*Articles, error)
}

type articleGetterService struct {
	c    client.Client
	name string
}

func NewArticleGetterService(name string, c client.Client) ArticleGetterService {
	if c == nil {
		c = client.NewClient()
	}
	if len(name) == 0 {
		name = "article"
	}
	return &articleGetterService{
		c:    c,
		name: name,
	}
}

func (c *articleGetterService) GetArticles(ctx context.Context, in *Filter, opts ...client.CallOption) (*Articles, error) {
	req := c.c.NewRequest(c.name, "ArticleGetter.GetArticles", in)
	out := new(Articles)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for ArticleGetter service

type ArticleGetterHandler interface {
	GetArticles(context.Context, *Filter, *Articles) error
}

func RegisterArticleGetterHandler(s server.Server, hdlr ArticleGetterHandler, opts ...server.HandlerOption) {
	type articleGetter interface {
		GetArticles(ctx context.Context, in *Filter, out *Articles) error
	}
	type ArticleGetter struct {
		articleGetter
	}
	h := &articleGetterHandler{hdlr}
	s.Handle(s.NewHandler(&ArticleGetter{h}, opts...))
}

type articleGetterHandler struct {
	ArticleGetterHandler
}

func (h *articleGetterHandler) GetArticles(ctx context.Context, in *Filter, out *Articles) error {
	return h.ArticleGetterHandler.GetArticles(ctx, in, out)
}
